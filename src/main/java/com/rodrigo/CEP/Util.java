package com.rodrigo.CEP;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by rodrigocavalcante on 8/17/14.
 */
@EBean
public class Util {

    @RootContext
    Activity context;

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        System.out.println(activeNetwork);
        if(activeNetwork != null)
            return true;

        return false;
    }

    public String unmaskCep(String maskedCEP)
    {
        maskedCEP = maskedCEP.replace(".","");
        return  maskedCEP.replace("-","");
    }

}

