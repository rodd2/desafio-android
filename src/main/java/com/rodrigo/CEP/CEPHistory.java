package com.rodrigo.CEP;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by rodrigocavalcante on 8/17/14.
 */
@EViewGroup(R.layout.history)
public class CEPHistory extends RelativeLayout {

    public CEPHistory(Context context) {
        super(context);
    }

    @ViewById(R.id.cep)
    TextView cep;

    @ViewById(R.id.lougradouro)
    TextView lougradoro;

    @ViewById(R.id.cidade)
    TextView cidade;

    @ViewById(R.id.bairro)
    TextView bairro;

    @ViewById(R.id.estado)
    TextView estado;

    @ViewById(R.id.tipoDeLougradouro)
    TextView tipoDeLogradouro;

    public void bind(CEP model) {
        cep.setText(model.getCepNumero());
        estado.setText(model.getEstado());
        cidade.setText(model.getCidade()+"-");
        bairro.setText(model.getBairro()+", ");
        tipoDeLogradouro.setText(model.getTipoDeLogradouro()+" ");
        lougradoro.setText(model.getLogradouro());
    }
}

