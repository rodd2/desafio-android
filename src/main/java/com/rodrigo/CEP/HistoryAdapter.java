package com.rodrigo.CEP;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by rodrigocavalcante on 8/17/14.
 */
@EBean
public class HistoryAdapter  extends BaseAdapter{

    private List<CEP> cepList;
    private Context context;

    public HistoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return cepList.size();
    }

    @Override
    public CEP getItem(int position) {
        return cepList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CEPHistory history;
        if (convertView == null) {
            history = com.rodrigo.CEP.CEPHistory_.build(this.context);
        } else {
            history = (CEPHistory) convertView;
        }

        history.bind(getItem(position));

        return history;
    }

    public void setCepList(List<CEP> cepList) {
        this.cepList = cepList;
    }
}

