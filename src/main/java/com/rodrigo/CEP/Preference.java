package com.rodrigo.CEP;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by rodrigocavalcante on 8/17/14.
 */
@SharedPref
public interface Preference {

    String ceps();
}

