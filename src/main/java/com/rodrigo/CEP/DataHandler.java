package com.rodrigo.CEP;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigocavalcante on 8/17/14.
 */

@EBean(scope = EBean.Scope.Singleton)
public class DataHandler {

    @Pref
    com.rodrigo.CEP.Preference_ pref;

    public void clear()
    {
        pref.clear();
    }

    public void saveCEP(CEP model)
    {
        List<CEP> ceps = getCeps();

        boolean cepsAlreadySaved = false;

        if(ceps != null) {
            for (CEP c : ceps) {
                if (c.getCepNumero().matches(model.getCepNumero()))
                    cepsAlreadySaved = true;
            }
        }

        if(!cepsAlreadySaved) {
            try {
                pref.ceps().put(pref.ceps().get() + "@" + model.toJSON());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<CEP> getCeps()
    {
        List<CEP> ceps = new ArrayList<CEP>();

        String jsons[] = pref.ceps().get().split("@");

        ObjectMapper mapper = new ObjectMapper();
        for (String json : jsons)
        {
            if(json.length() > 0) {
                try {
                    CEP c = mapper.readValue(json, CEP.class);
                    ceps.add(c);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return ceps;
    }

}
