package com.rodrigo.CEP;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Rest(rootUrl = "http://correiosapi.apphb.com/cep/", converters = { MappingJacksonHttpMessageConverter.class   })
@Accept(MediaType.APPLICATION_JSON)

public interface WebService {
    RestTemplate getWebService();

    @Get("{cep}")
    CEP getCEP(String cep);
}

