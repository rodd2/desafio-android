package com.rodrigo.CEP;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.androidannotations.annotations.*;
import org.androidannotations.annotations.rest.RestService;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;

@EActivity(R.layout.main)
public class MainActivity extends Activity {

    private TextWatcher mask;

    @ViewById(R.id.cep)
    EditText cepEt;

    @ViewById(R.id.search)
    Button searchBt;

    @ViewById(R.id.response)
    TextView returnTv;

    @Bean
    Util util;

    @RestService
    WebService webService;

    @Bean
    DataHandler dataHandler;

    @AfterViews
    void setMask() {
        mask = Mask.insert("##.###-###", cepEt);
        cepEt.addTextChangedListener(mask);
    }

    @Click
    void history()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setTitle(R.string.historico_popup_title);

        HistoryAdapter adapter = new HistoryAdapter(getApplicationContext());
        adapter.setCepList(dataHandler.getCeps());
        builder.setAdapter(adapter, null);

        AlertDialog alert = builder.create();

        alert.show();
    }

    @Click
    void search() {
        String typedCEP = cepEt.getText().toString();
        showMessage("");
        if(util.isConnected())
        {
            if(typedCEP.length() == 10) {
                showMessage("Buscando cep... Aguarde");
                lockUI();
                getCEPinBackgroud(typedCEP);
            }else
                showMessage("Cep incompleto. Preencha o cep no formato EX: 66.000-00");
        }
        else
            showMessage("Por favor, verifique a sua conexão com a internet.");
    }

    @Background
    void getCEPinBackgroud(String typedCEP)
    {
        CEP model = getCEP(typedCEP);

        if(model != null) {
            showMessage("CEP ENCONTRADO:\n" + model.toString());
            dataHandler.saveCEP(model);
        }
    }

    CEP getCEP(String typedCEP)
    {
        CEP model = null;
        try {
            model = webService.getCEP(util.unmaskCep(typedCEP));
        } catch (HttpClientErrorException e) {
            HttpStatus status = e.getStatusCode();
            if (status == HttpStatus.NOT_FOUND)
                showMessage("CEP NÃO ENCONTRADO.");
            else
                showMessage("HOUVE UM ERRO NO SERVIDOR.\nTENTE NOVAMENTE");
        }

        unlockUI();

        return model;
    }

    @UiThread
    void showMessage(String msg)
    {
        returnTv.setText(msg);
    }

    @UiThread
    void lockUI()
    {
        searchBt.setEnabled(false);
        cepEt.setEnabled(false);
    }

    @UiThread
    void unlockUI()
    {
        searchBt.setEnabled(true);
        cepEt.setEnabled(true);
    }
}
